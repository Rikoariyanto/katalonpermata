import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/View_Product Parameter/span_Product Parameter'))

'Click add button '
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/add btn'))

WebUI.delay(3)

WebUI.takeScreenshot()

'Click data source, choose data'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inpt_data source'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inpt_data source'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inpt_product group'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inpt_product group'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, 
        Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inpt_product code'))

'Input product code "test"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inpt_product code'), 'test')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inp_aqad type'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inp_aqad type'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inp_repayment type'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inp_repayment type'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inp_instrument cls'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inp_instrument cls'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inp_business group'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inp_business group'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inpt_product description'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/inpt_product description'), 
    'test')

WebUI.check(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/chk_staff loan'))

WebUI.check(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/Chk_IA asset exclude'))

WebUI.delay(3)

WebUI.check(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/chk_UD calculate'))

WebUI.delay(3)

WebUI.uncheck(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/chk_is impared'))

WebUI.delay(3)

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Add_Product Parameter/btn_submit'))

WebUI.delay(6)

WebUI.takeScreenshot()

