import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/View_Product Parameter/span_Product Parameter'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/fld_search prdcode'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/fld_search prdcode'), 'test')

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/fld_search prdcode'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/btn_edit'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_aqadtype'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_aqadtype'), Keys.chord(
        Keys.ARROW_UP, Keys.ARROW_UP, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_repaymenttype'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_repaymenttype'), Keys.chord(
        Keys.ARROW_UP, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_instrument cls'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_instrument cls'), Keys.chord(
        Keys.ARROW_UP, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_business group'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_business group'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_prddesc'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/inp_prddesc'), 'coba test')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/chk_is impaired'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/chk_ia asset exclude'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/chk_stafloan'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/chk_ud calculate'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Product Parameter/Edit_Product Parameter/btn_submitedt'))

WebUI.delay(3)

WebUI.takeScreenshot()

