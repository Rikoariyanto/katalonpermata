import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Obj_PSAK GL Group Parameter View'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Add_PSAK GL Group Parameter/Add'))

WebUI.delay(2)

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Add_PSAK GL Group Parameter/Input_journal_type'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Add_PSAK GL Group Parameter/Input_journal_type'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Add_PSAK GL Group Parameter/Input_GL_group'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Add_PSAK GL Group Parameter/Input_GL_group'), 
    'TEST01')

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Add_PSAK GL Group Parameter/Input_acod_db'), 
    '1111111')

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Add_PSAK GL Group Parameter/Input_acod_cr'), 
    '2222222')

WebUI.delay(2)

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_PSAK GL Group Parameter/Add_PSAK GL Group Parameter/Submit_btn'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.switchToDefaultContent()

