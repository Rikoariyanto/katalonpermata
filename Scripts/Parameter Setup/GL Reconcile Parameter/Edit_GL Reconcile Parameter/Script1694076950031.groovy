import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Obj_Parameter Setup/Obj_GL Reconcile Parameter/View_GL Reconcile Parameter/img_Scenario Rule Setting_dxtv-btn'))

'Click Menu GL Reconcile Parameter'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/View_GL Reconcile Parameter/span_GL Reconcile Parameter'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_srch gl number'))

'Input "62111" in field Search GL Number'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_srch gl number'), 
    '62111')

WebUI.delay(3)

WebUI.takeScreenshot()

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_srch gl number'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/btn_edit'))

'Choose GL Group "OFF_BS - Off Balance Sheet"'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_gl group dl'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_gl group dl'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ENTER))

'Choose ACOD Principal "20012"'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_acod principal dl'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_acod principal dl'), 
    Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_gl decs'))

'Input GL Description "GL Coba Reconcile1"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/inp_gl decs'), 
    'GL Coba Reconcile1')

'Uncheck list ABS Amount'
WebUI.check(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/chk_abs amount'))

WebUI.delay(3)

'Click button Submit'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_GL Reconcile Parameter/Edit_GL Reconcile Parameter/btn_submit edit'))

