import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Value Center Group Parameter/Obj_Value Center Group Parameter View'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Value Center Group Parameter/Edit_Value Center Group Parameter/Input_filter_data'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Value Center Group Parameter/Edit_Value Center Group Parameter/Input_filter_data'), 
    'WB')

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Value Center Group Parameter/Edit_Value Center Group Parameter/Input_filter_data'), 
    Keys.chord(Keys.ENTER), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Value Center Group Parameter/Edit_Value Center Group Parameter/Edit'))

WebUI.delay(2)

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Value Center Group Parameter/Edit_Value Center Group Parameter/Edit_value_center_code'), 
    '007')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Value Center Group Parameter/Add_Value Center Group Parameter/Submit_btn'))

WebUI.delay(2)

WebUI.takeScreenshot()

