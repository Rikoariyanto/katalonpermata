import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Click Menu "Journal Parameter"'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/View_Journal Parameter/span_Journal Parameter'))

'Klik Button Add'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/btn_add'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_ifrs cost value'))

'Input IFRS Cost Value "Fee"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_ifrs cost value'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl group'))

WebUI.delay(3)

'Input GL Group "GL PD"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl group'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ENTER, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_ifrs'))

'Input IFRS Account Type "ITRCG - Initial Recognition EIR"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_ifrs'), Keys.chord(Keys.ARROW_DOWN, 
        Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_jurnal desc'))

'Input Journal Description "GL Cost Test"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_jurnal desc'), 'GL Cost Test')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl acc db'))

'Input Account DB "620010"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl acc db'), '620010')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl acc cr'))

'Input Account CR "620020"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl acc cr'), '620020')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl internal code db'))

'Input GL Internal Code DB "GL Test Debit"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl internal code db'), 
    'GL Test Debit')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl internal code cr'))

'Input GL Internal Code CR "GL Test Credit"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/inp_gl internal code cr'), 
    'GL Test Credit')

'Check list branch IFRS'
WebUI.check(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/chk_branch ifrs'))

WebUI.delay(3)

'Click button "Submit"'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Add_Journal Parameter/btn_submit'))

