import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/View_Journal Parameter/span_Journal Parameter'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/search_jurnal desc'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/search_jurnal desc'), 'GL Cost Test')

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/search_jurnal desc'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/btn_edit'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_jurnal desc'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_jurnal desc'), 'GL Cost Test')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_gl acc db'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_gl acc db'), '620010')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_gl acc cr'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_gl acc cr'), '620020')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_internal code db'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_internal code db'), 
    'GL Test Debit')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_internal code cr'))

WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/inp_internal code cr'), 
    'GL Test Credit')

WebUI.uncheck(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/chk_branch ifrs'))

WebUI.delay(3)

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Journal Parameter/Edit__Journal Parameter/btn_submit edit'))

WebUI.delay(3)

