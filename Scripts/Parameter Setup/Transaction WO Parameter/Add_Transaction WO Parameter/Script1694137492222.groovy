import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Click Transaction Wo Parameter Menu'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/View_Transaction WO Parameter/span_Transaction WO Parameter'))

'Click button Add'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/btn_add'))

WebUI.delay(3)

WebUI.takeScreenshot()

'Input Data Source "JF"'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_data source'))

WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_data source'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_transac classif'))

'Input Transaction Classification "WO"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_transac classif'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_sign'))

'Input Sign "-"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_sign'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_transac flag dl'))

'Input Transaction Flag "1- Active"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_transac flag dl'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_debit credit flag'))

'Input Debit Credit Flag "D"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_debit credit flag'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_transaction code'))

'Input Transactio Code "T01"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_transaction code'), 
    'T01')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_transaction desc'))

'Input Transaction Description "Transaction Test"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_transaction desc'), 
    'Transaction Test')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_acod'))

'Input acod number "62021"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/inp_acod'), 
    '62021')

'Check list journal flag'
WebUI.check(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/chk_journal flag'))

WebUI.delay(3)

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Add_Transaction WO Parameter/btn_submit'))

WebUI.delay(6)

