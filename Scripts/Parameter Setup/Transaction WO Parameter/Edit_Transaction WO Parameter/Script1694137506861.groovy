import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Click Transaction Wo Parameter Menu'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/View_Transaction WO Parameter/span_Transaction WO Parameter'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/search_trx code'))

'Click TRX Code T01 in column search by field'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/search_trx code'), 
    'T01')

WebUI.delay(3)

WebUI.takeScreenshot()

'Click Enter'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/search_trx code'), 
    Keys.chord(Keys.ENTER))

'Click button Edit'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/btn_edit'))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_data source dl'))

'Input Data Source'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_data source dl'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_trans class'))

'Input Transaction Classification'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_trans class'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_sign'))

'Input Sign "-"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_sign'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_trans flag'))

'Input Transaction Flag '
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_trans flag'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_DB flag'))

'Input Debit Credit Flag "D"'
WebUI.sendKeys(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_DB flag'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_trans code'))

'Input Transactio Code "T012"'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_trans code'), 
    'T012')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_trans desc'))

'Input Transaction Description '
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_trans desc'), 
    'Transaction Test1')

WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_acod'))

'Input acod number'
WebUI.setText(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/inp_acod'), 
    '62022')

'Un Check list Journal Flag'
WebUI.check(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/chk_journal flag'))

WebUI.delay(3)

'Click button submit'
WebUI.click(findTestObject('Obj_Parameter Setup/Obj_Transaction WO Parameter/Edit_Transaction WO Parameter/btn_submit'))

WebUI.delay(6)

