import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Add'))

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_username'))

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_module'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_module'), 'IFRS 9')

WebUI.sendKeys(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_module'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_branchgroup'))

WebUI.setText(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_branchgroup'), '1-All_Branch')

WebUI.sendKeys(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_branchgroup'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_rolegroup'))

WebUI.sendKeys(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_rolegroup'), Keys.chord(Keys.ARROW_DOWN, 
        Keys.ARROW_DOWN, Keys.ENTER))

WebUI.check(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_reportto'))

WebUI.setText(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_username'), 'user07')

WebUI.sendKeys(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_fullname'), 'Gya')

WebUI.delay(3)

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Save_btn'))

WebUI.waitForAlert(3)

WebUI.verifyAlertPresent(3)

WebUI.acceptAlert()

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.switchToDefaultContent()

