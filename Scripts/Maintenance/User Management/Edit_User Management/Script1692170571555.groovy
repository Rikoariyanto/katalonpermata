import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Obj_User Management View'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_filter_data'))

WebUI.setText(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_filter_data'), 'user07')

WebUI.clickOffset(findTestObject('Obj_Maintenance/Obj_User Management/Add_User Management/Input_filter_data'), 10, 10)

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Edit_User Management/Edit'))

WebUI.delay(2)

WebUI.setText(findTestObject('Obj_Maintenance/Obj_User Management/Edit_User Management/Edit_fullname'), 'Gya-edit')

WebUI.click(findTestObject('Obj_Maintenance/Obj_User Management/Edit_User Management/Save_btn'))

WebUI.acceptAlert()

WebUI.takeScreenshot()

WebUI.delay(2)

WebUI.switchToDefaultContent()

