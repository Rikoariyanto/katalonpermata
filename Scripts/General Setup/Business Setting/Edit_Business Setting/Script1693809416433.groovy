import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_filter_value1'))

WebUI.setText(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_filter_value1'), '1')

WebUI.sendKeys(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_filter_value1'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/btn_edit'))

WebUI.delay(2)

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_value2'))

WebUI.setText(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_value2'), '3')

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_value3'))

WebUI.setText(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_value3'), '4')

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_description'))

WebUI.setText(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_description'), 'test edit')

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_ParentCommonCode'))

WebUI.sendKeys(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_ParentCommonCode'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_ParentCommonCodeValue'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_ParentCommonCodeValue'), 
    Keys.chord(Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_sequence'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/input_sequence'), '4')

WebUI.click(findTestObject('Obj_General Setup/Obj_Business Setting/Edit_Business Setting/btn_submit'))

WebUI.delay(4)

WebUI.takeScreenshot()

