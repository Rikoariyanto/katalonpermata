import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/filter_SourceTable'))

WebUI.setText(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/filter_SourceTable'), 'header')

WebUI.click(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/btn_edit'))

WebUI.delay(2)

WebUI.click(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/input_SourceTable'))

WebUI.sendKeys(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/input_SourceTable'), Keys.chord(
        Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.delay(5)

WebUI.click(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/input_field'))

WebUI.sendKeys(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/input_field'), Keys.chord(Keys.ARROW_DOWN, 
        Keys.ARROW_DOWN, Keys.ARROW_DOWN, Keys.ENTER))

WebUI.delay(3)

WebUI.click(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/input_DataRetention'))

WebUI.setText(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/input_DataRetention'), '3')

WebUI.delay(2)

WebUI.click(findTestObject('Obj_General Setup/Obj_Master Retention/Edit_Master Retention/btn_submit'))

WebUI.acceptAlert()

