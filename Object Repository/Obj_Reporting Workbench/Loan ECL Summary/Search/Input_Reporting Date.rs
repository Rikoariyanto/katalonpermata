<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_Reporting Date</name>
   <tag></tag>
   <elementGuidId>bebc50d0-bd11-4a1f-9d3b-983ab71be26c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ctContent_dteDatePeriod_I&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Obj_Parent/Obj_For_Parent</value>
      <webElementGuid>e6b6ac6c-6fde-463b-9c92-36b4f2733685</webElementGuid>
   </webElementProperties>
</WebElementEntity>
