<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Export_btn</name>
   <tag></tag>
   <elementGuidId>fed33b46-5c0b-4a73-b6c4-6aa1a81b4b50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ctContent_uclParam_uclExporter_popupExport_btnExport&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Obj_Parent/Obj_for_parent</value>
      <webElementGuid>0ae3da5f-92e3-4a2e-a4af-7e2e6681fc6b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
