<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Market Rate Parameter</name>
   <tag></tag>
   <elementGuidId>f6c12a84-e36a-42ff-9d83-eeb544ba378f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#treeView_N1_3 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N1_3']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4c04f938-e405-46b6-a249-973493f6e0ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>a951ee37-1cf3-464f-a701-28d457f5ff8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Market Rate Parameter</value>
      <webElementGuid>a9a10206-e5ac-4687-826d-bda9049ff9c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N1_3&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>68140d94-9777-46ae-8d7d-8bb7d934d6f6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N1_3']/span</value>
      <webElementGuid>c03cd354-7141-4945-affd-b48bf22d96a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transaction Parameter'])[1]/following::span[2]</value>
      <webElementGuid>01d8a663-4ba9-4187-ac2e-4e0a4d7ddc24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Journal Parameter'])[1]/following::span[4]</value>
      <webElementGuid>4d4c6f6d-d685-4455-872d-52efb934e392</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Classification and Measurement'])[1]/preceding::span[2]</value>
      <webElementGuid>c7d95ef0-4903-48fa-bf89-394dd45a17e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SPPI Setup'])[1]/preceding::span[4]</value>
      <webElementGuid>baad9250-a35b-43ee-ac18-1c3fa3cb65b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Market Rate Parameter']/parent::*</value>
      <webElementGuid>2f3f6c64-69a8-4d88-b51c-11d2e2eaebbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/ul/li[4]/a/span</value>
      <webElementGuid>5cdd4f59-83e4-4a46-88f7-648f97c2df24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Market Rate Parameter' or . = 'Market Rate Parameter')]</value>
      <webElementGuid>5ab5a1cc-eb09-49de-baa2-347f76e998ff</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
