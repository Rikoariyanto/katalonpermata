<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_GL Reconcile Parameter</name>
   <tag></tag>
   <elementGuidId>3b2e1569-fa29-4ca8-9c16-e0cb9b17e484</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#treeView_N1_3 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;treeView_N1_3&quot;]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>fa217da3-b6d2-4f95-aa67-164a94c97ab7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>a73ef0ab-ac9b-47fd-9f97-1cc63ea4e679</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>GL Reconcile Parameter</value>
      <webElementGuid>f9b2e714-e18a-44e7-97b2-95b2f46857fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N1_3&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>e288fa09-d69a-43aa-9f28-6d5fde109a17</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N1_3']/span</value>
      <webElementGuid>402497bb-7113-4cae-be91-82baaa37e7a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transaction WO Parameter'])[1]/following::span[2]</value>
      <webElementGuid>cfec71f6-fa94-4475-8cbb-82112f5705c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Journal Parameter'])[1]/following::span[4]</value>
      <webElementGuid>329f35d7-d1d4-4126-9bc2-8ed2acbc9794</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collateral Haircut Parameter'])[1]/preceding::span[2]</value>
      <webElementGuid>029f61ba-7215-47f5-874f-34701896c949</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Value Center Group Parameter'])[1]/preceding::span[4]</value>
      <webElementGuid>ff922698-cb17-4673-970c-9772a9e56d45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='GL Reconcile Parameter']/parent::*</value>
      <webElementGuid>3edba88d-439a-46ca-bc36-002f21b60fb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/ul/li[4]/a/span</value>
      <webElementGuid>310c93c6-2c6e-4560-8fcb-61f98292e2a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'GL Reconcile Parameter' or . = 'GL Reconcile Parameter')]</value>
      <webElementGuid>97b3a22a-cfb5-4124-b4e6-54365bb48933</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
