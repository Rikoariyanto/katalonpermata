<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_export</name>
   <tag></tag>
   <elementGuidId>46fdf60a-d6e2-4c63-9d94-1518df505f80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ctContent_uclParam_uclExporter_lnkExportXLS_dgData&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Obj_Parent/Obj_for_parent</value>
      <webElementGuid>a0ee5be0-cdff-4ae1-90af-d57c0bfd76bc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
