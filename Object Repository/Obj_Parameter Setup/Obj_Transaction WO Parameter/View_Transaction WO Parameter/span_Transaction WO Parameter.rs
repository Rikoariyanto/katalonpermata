<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Transaction WO Parameter</name>
   <tag></tag>
   <elementGuidId>93df22a8-89e9-4fbe-bbc1-6ba83432c943</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#treeView_N1_2 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;treeView_N1_2&quot;]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>133bb32e-29bd-4a9f-89bc-d2dcc187240d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>9d4a3189-1b84-4e9d-902a-36db11b8d4d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Transaction WO Parameter</value>
      <webElementGuid>dadf5568-c77d-40bd-9151-0b771cb36097</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N1_2&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>5075accf-8c57-45a0-a7bf-20fb597556b0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N1_2']/span</value>
      <webElementGuid>32dbc480-6004-4dd2-896d-f6494dace505</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Journal Parameter'])[1]/following::span[2]</value>
      <webElementGuid>61d222f7-7706-490c-8704-d7ebdee87ba7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product Parameter'])[1]/following::span[4]</value>
      <webElementGuid>b3e20219-3f59-4151-8d90-f4a818bf993e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='GL Reconcile Parameter'])[1]/preceding::span[2]</value>
      <webElementGuid>68906212-bdd0-45b4-a56c-83ba6f308d99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collateral Haircut Parameter'])[1]/preceding::span[4]</value>
      <webElementGuid>c1adda3b-3a2f-4edd-bfcd-309035a6a571</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Transaction WO Parameter']/parent::*</value>
      <webElementGuid>377f58c6-da7f-413b-9a85-e705ad62f141</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/ul/li[3]/a/span</value>
      <webElementGuid>31ac91ba-7824-4c4d-876a-4f5b7b697241</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Transaction WO Parameter' or . = 'Transaction WO Parameter')]</value>
      <webElementGuid>ccddf4f0-9c31-4e0c-876a-5575f3506401</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
