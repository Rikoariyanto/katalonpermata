<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_export</name>
   <tag></tag>
   <elementGuidId>8c0556bc-9d80-40f2-bf47-81426b26b491</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ctContent_uclParam_uclExporter_lnkExportXLS_dgData&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Obj_Parent/Obj_for_parent</value>
      <webElementGuid>b71ce163-158d-407d-9c8f-327c91544694</webElementGuid>
   </webElementProperties>
</WebElementEntity>
