<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Journal Parameter</name>
   <tag></tag>
   <elementGuidId>e5b78b71-3520-4f50-965b-15e5b65a7e0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#treeView_N1_1 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N1_1']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>25ec726e-c5f2-4dec-a24e-ea69f71fea3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>613161b9-0ea6-4d03-8f63-2342462ca280</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Journal Parameter</value>
      <webElementGuid>15d602d0-3ad1-4362-a62c-15f9f66ac45e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N1_1&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>cd531975-71f7-4d82-8753-046d86873338</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N1_1']/span</value>
      <webElementGuid>82ce44ab-0855-463a-b364-4c27c58d863a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product Parameter'])[1]/following::span[2]</value>
      <webElementGuid>f76997c4-b311-42ca-9977-a82d6cfaa292</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Parameter Setup'])[1]/following::span[4]</value>
      <webElementGuid>1aa4d0ea-8488-4c5b-81c5-4829e8cb8972</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transaction Parameter'])[1]/preceding::span[2]</value>
      <webElementGuid>d7ea0391-e182-4feb-87bb-9f2b0152c53b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Market Rate Parameter'])[1]/preceding::span[4]</value>
      <webElementGuid>4817007c-5862-4a0a-9fbc-82da302ced83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Journal Parameter']/parent::*</value>
      <webElementGuid>da8646c1-b5e7-45a4-9354-e554e5d74a64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/ul/li[2]/a/span</value>
      <webElementGuid>f26693ec-a1dc-41db-bfca-faa13221fef0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Journal Parameter' or . = 'Journal Parameter')]</value>
      <webElementGuid>aab8e075-cb12-4ecb-bd45-36161a5cd375</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
