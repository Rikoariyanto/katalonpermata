<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Macro Economic Model</name>
   <tag></tag>
   <elementGuidId>8340e85f-665d-44c3-a958-195da4c8b14e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_2_1']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_2_1 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>3485b9d4-6964-425d-b958-39c48e91a2cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>416afea1-7585-4c3a-ac7d-81a68e986e99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Macro Economic Model</value>
      <webElementGuid>298d7192-556e-4310-b716-3c0e072e042b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_2_1&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>1f3c743d-a40e-46bf-a2a9-9febf26396e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_2_1']/span</value>
      <webElementGuid>d16f4733-6865-4626-b4b6-e4d72cdedcf2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Macro Economic Registration'])[1]/following::span[2]</value>
      <webElementGuid>228afccc-79d8-4822-93eb-4f6006f8b57d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Macro Economic Configuration'])[1]/following::span[4]</value>
      <webElementGuid>da25a305-c12a-4b8e-9894-8df5138fea1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Parameter Arima Model'])[1]/preceding::span[2]</value>
      <webElementGuid>3ef72e63-d16b-4a9b-b2fb-0efcb5e3c4db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Configuration'])[1]/preceding::span[4]</value>
      <webElementGuid>f9f42d70-e2dc-48ad-8d6a-44ae88501122</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Macro Economic Model']/parent::*</value>
      <webElementGuid>59238826-246f-469c-ad85-bde87dfb98dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul/li[2]/a/span</value>
      <webElementGuid>8ade321d-c665-433f-b4b0-d5e7ce9ed57d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Macro Economic Model' or . = 'Macro Economic Model')]</value>
      <webElementGuid>c621d827-194f-4827-9b3b-baf26f261c64</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
