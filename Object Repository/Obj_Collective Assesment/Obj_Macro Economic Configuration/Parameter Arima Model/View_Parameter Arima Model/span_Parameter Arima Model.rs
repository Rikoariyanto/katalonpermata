<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Parameter Arima Model</name>
   <tag></tag>
   <elementGuidId>87f0a215-a58c-4ef2-a643-9bf3f52ad331</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_2_2']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_2_2 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>bdb7462e-5f36-4551-94d7-92e687234fd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>eef67d3d-5db9-4fbd-accb-ad3b450c1406</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Parameter Arima Model</value>
      <webElementGuid>db302363-40a6-46d0-a1eb-f2579c3c9562</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_2_2&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>f3a2111c-c944-44b5-808e-3317a325f65f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_2_2']/span</value>
      <webElementGuid>35a3f4d8-5fe4-4abc-9b58-18d13e014bc9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Macro Economic Model'])[1]/following::span[2]</value>
      <webElementGuid>f5972951-338e-41d4-9df7-54969211721f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Macro Economic Registration'])[1]/following::span[4]</value>
      <webElementGuid>9a80c919-b1d6-4d14-b4e3-66dc9d00ce30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Configuration'])[1]/preceding::span[2]</value>
      <webElementGuid>3ced9ade-2b7a-44b0-b090-2fb8a3e71275</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Transition Matrix Configuration'])[1]/preceding::span[4]</value>
      <webElementGuid>5e87e839-2828-4edf-8cac-fd87c66a6af1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Parameter Arima Model']/parent::*</value>
      <webElementGuid>1c013d20-ec5a-4255-ab56-c118195173b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul/li[3]/a/span</value>
      <webElementGuid>b8f7ea22-db2b-43fd-81af-3855a3371fb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Parameter Arima Model' or . = 'Parameter Arima Model')]</value>
      <webElementGuid>e1c1bbd9-7521-47ac-af37-195dbbebf50b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
