<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Macro Economic Registration</name>
   <tag></tag>
   <elementGuidId>0b65e916-9278-44d7-92a0-7d24c8f3d65b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_2_0']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_2_0 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>eba6b63a-c0cd-41a5-a40b-b80a07ade9b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>3916d190-d743-4efc-8131-1c38a62ebb45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Macro Economic Registration</value>
      <webElementGuid>150927c3-390e-4123-b04f-44674fc8f4dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_2_0&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>4ecd29cb-c694-4d94-8470-50bdef15c3e8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_2_0']/span</value>
      <webElementGuid>6baebe55-d113-48b8-92a6-ae9367fee3d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Macro Economic Configuration'])[1]/following::span[2]</value>
      <webElementGuid>7df701c7-1808-4fc9-8d36-1cdbda46f962</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Group Bucket Maintenance'])[1]/following::span[4]</value>
      <webElementGuid>acc384b9-d17c-4c07-925a-06b3af7c776f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Macro Economic Model'])[1]/preceding::span[2]</value>
      <webElementGuid>9fb0aed5-46a4-4f81-a43b-0a144122903b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Parameter Arima Model'])[1]/preceding::span[4]</value>
      <webElementGuid>8553883b-df80-4061-a3f5-284fd4275c08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Macro Economic Registration']/parent::*</value>
      <webElementGuid>4b8d5b5d-0b93-493a-87b7-b16512ae21a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul/li/a/span</value>
      <webElementGuid>63df3a58-69a8-4cda-8e74-56c8274600f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Macro Economic Registration' or . = 'Macro Economic Registration')]</value>
      <webElementGuid>a0deff5f-7c99-4546-a77a-f4ed82dceba7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
