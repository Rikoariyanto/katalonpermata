<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_LGD Configuration</name>
   <tag></tag>
   <elementGuidId>385563db-4619-48b4-bec4-1e5b50266089</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_4']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_4 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b23eaea2-fd61-4131-bda6-f6399152f331</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>f80f4808-e693-4c59-bdef-3e3101df1e9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>LGD Configuration</value>
      <webElementGuid>63fabd00-cf43-4fdc-89f9-9ce683d553da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_4&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>1058ad72-2a4b-4545-b2da-27dc3e870bce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_4']/span</value>
      <webElementGuid>a39a2f64-24b7-4943-9abf-2bf23664060e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Forward Looking Configuration'])[1]/following::span[2]</value>
      <webElementGuid>f9146054-df9d-48fc-a2ad-42d1a80dc71a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Transition Matrix Configuration'])[1]/following::span[4]</value>
      <webElementGuid>4708b21d-89b3-40dc-bb40-b3d01bacaabe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EAD Configuration'])[1]/preceding::span[2]</value>
      <webElementGuid>87b0a2f4-6c90-42de-a32b-4a417e59901c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='UD Configuration'])[1]/preceding::span[4]</value>
      <webElementGuid>a148101f-fb16-47fc-a3cf-fd462aba3a23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='LGD Configuration']/parent::*</value>
      <webElementGuid>dea11e6e-8e54-4ada-ba0e-ea4a73c8d6d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/ul/li[5]/a/span</value>
      <webElementGuid>a1031a59-053e-49c3-9f12-20f0f79db07a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'LGD Configuration' or . = 'LGD Configuration')]</value>
      <webElementGuid>6761556d-d133-4a5f-adf7-4b258c2dd2c3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
