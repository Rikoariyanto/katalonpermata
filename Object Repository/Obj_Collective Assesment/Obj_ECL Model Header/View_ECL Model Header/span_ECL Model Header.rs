<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_ECL Model Header</name>
   <tag></tag>
   <elementGuidId>ca3487e6-1903-40aa-b9bd-5eac01a77d2c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_6']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_6 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2bb614ac-5e3c-443e-9d3a-da440f6b8837</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>12d428c7-bf6f-4872-b6bb-b97a9130805a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ECL Model Header</value>
      <webElementGuid>74d28a8f-8738-4762-9241-4f7096cb138c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_6&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>3406795b-b729-449b-9340-1b6c5343b101</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_6']/span</value>
      <webElementGuid>afd0698d-b8e5-42c4-9409-12c72bdf949a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PR Configuration'])[1]/following::span[2]</value>
      <webElementGuid>c8b15029-47f9-4719-9f85-77772e210830</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EAD Model Configuration'])[1]/following::span[4]</value>
      <webElementGuid>8e2c0e1c-26da-4d69-8feb-d47cbf9fe79c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pending Model Simulation'])[1]/preceding::span[2]</value>
      <webElementGuid>28d15607-05dc-49e9-b347-8c04c3f8f3f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reporting Workbench'])[1]/preceding::span[4]</value>
      <webElementGuid>e7ddace7-276f-42d8-9113-1c12b2b1eb9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ECL Model Header']/parent::*</value>
      <webElementGuid>0831d36f-3098-4dd1-b825-9cc1ba60e3b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/ul/li[7]/a/span</value>
      <webElementGuid>d9918fc3-58c2-4589-ac54-017e3a9c4dc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'ECL Model Header' or . = 'ECL Model Header')]</value>
      <webElementGuid>176e67c6-be6b-4146-b5e7-853ce035d215</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
