<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_PD Transition Matrix Configuration</name>
   <tag></tag>
   <elementGuidId>0349da73-0ab1-40cd-8374-e25045fea458</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_3_0']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_3_0 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6a55e04f-27a5-4f02-8e74-e29aa437196e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>47712a66-ae5b-489b-a4ee-362047b6e9df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PD Transition Matrix Configuration</value>
      <webElementGuid>4307b641-d158-4cb4-a864-41a6bf75a5be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_3_0&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>e9e7e6b0-785a-44ee-a9a9-42e7e50fe90e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_3_0']/span</value>
      <webElementGuid>6cc982f5-64fb-4aba-9870-49c17cf883e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Configuration'])[1]/following::span[2]</value>
      <webElementGuid>847f4d1b-b3fa-45c7-9807-d79ac7209564</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Parameter Arima Model'])[1]/following::span[4]</value>
      <webElementGuid>ca72212a-5a48-4c4a-8c3c-cd088839c981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Forward Looking Configuration'])[1]/preceding::span[2]</value>
      <webElementGuid>1a879124-6d96-4a5e-be46-91bb98e09f40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LGD Configuration'])[1]/preceding::span[4]</value>
      <webElementGuid>4609fe86-0aac-469b-83f4-652edfa52b67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='PD Transition Matrix Configuration']/parent::*</value>
      <webElementGuid>5a0bfe54-bf8a-4d35-be70-98a78ed1ee89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/ul/li[4]/ul/li/a/span</value>
      <webElementGuid>d74e3ffb-d786-4baa-a1b7-01cfa2de5c6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'PD Transition Matrix Configuration' or . = 'PD Transition Matrix Configuration')]</value>
      <webElementGuid>4715bd01-b75b-4bd4-9889-02cd13fbe650</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
