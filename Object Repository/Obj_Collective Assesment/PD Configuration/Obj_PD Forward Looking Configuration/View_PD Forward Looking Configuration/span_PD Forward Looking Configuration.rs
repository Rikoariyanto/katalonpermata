<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_PD Forward Looking Configuration</name>
   <tag></tag>
   <elementGuidId>afb34233-51f5-41d5-ba1e-32c492ee9fff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_3_1']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_3_1 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>88f99086-c560-404d-8c73-e2dfbb32e7e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>c21a52c3-b609-4802-8f43-71c33dd253f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PD Forward Looking Configuration</value>
      <webElementGuid>8c59dbd6-dfdb-4547-8ed1-cd24bc3a74f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_3_1&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>611bc949-b3b4-4fe2-88bd-e1561057ede4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_3_1']/span</value>
      <webElementGuid>d29525dd-ceec-4a89-a716-708caa47b8a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Transition Matrix Configuration'])[1]/following::span[2]</value>
      <webElementGuid>5d277729-f18c-4cc4-8cc3-54c9eb391680</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PD Configuration'])[1]/following::span[4]</value>
      <webElementGuid>f41d934d-38c2-46a1-888f-15a8248a987c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LGD Configuration'])[1]/preceding::span[2]</value>
      <webElementGuid>87754a2d-b047-4406-b095-419448045750</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EAD Configuration'])[1]/preceding::span[4]</value>
      <webElementGuid>c7e66f41-f1cb-4f66-a26f-934bec46b2c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='PD Forward Looking Configuration']/parent::*</value>
      <webElementGuid>22460199-9157-4a00-9177-f1277694201a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/ul/li[4]/ul/li[2]/a/span</value>
      <webElementGuid>2f60b3bf-b8d6-4b84-b0a4-7d5e8fff810b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'PD Forward Looking Configuration' or . = 'PD Forward Looking Configuration')]</value>
      <webElementGuid>9b1ad616-2735-4742-88f6-d47f5a16ea62</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
