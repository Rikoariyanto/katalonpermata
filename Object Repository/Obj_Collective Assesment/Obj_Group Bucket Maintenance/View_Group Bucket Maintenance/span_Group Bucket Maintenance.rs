<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Group Bucket Maintenance</name>
   <tag></tag>
   <elementGuidId>2e4db58e-5b40-43b2-aa9b-0dab6d8d2350</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_1']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_1 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a5d53761-6067-4ac8-b778-8bd621dbfc6f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>06cf35ab-f04f-419a-8d40-38a731084bef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Group Bucket Maintenance</value>
      <webElementGuid>b2f5632f-9a24-42d3-90bf-3eded99ea43b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_1&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>c18c0360-5e6a-4717-ab62-9082defea65d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_1']/span</value>
      <webElementGuid>9626789d-d31b-43ae-b372-5cab1bbc80eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Segmentation Configuration'])[1]/following::span[2]</value>
      <webElementGuid>f84cb53c-3b22-4d13-bc6e-18087bcf987e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collective Assessment'])[1]/following::span[4]</value>
      <webElementGuid>9690a4f1-b9b0-4f67-972d-6c67b3672380</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Macro Economic Configuration'])[1]/preceding::span[2]</value>
      <webElementGuid>5d5d8202-b7ef-43b4-890b-5efa42def015</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Macro Economic Registration'])[1]/preceding::span[4]</value>
      <webElementGuid>fec6b466-6cdd-4085-ae8c-5d356214191e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Group Bucket Maintenance']/parent::*</value>
      <webElementGuid>36cadb48-55fe-4d11-84c4-4015c5b581f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/ul/li[2]/a/span</value>
      <webElementGuid>06b79ae2-7563-4f43-99e6-f2a4f8a105a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Group Bucket Maintenance' or . = 'Group Bucket Maintenance')]</value>
      <webElementGuid>8cf5efed-862f-4618-999f-6245e6570ff5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
