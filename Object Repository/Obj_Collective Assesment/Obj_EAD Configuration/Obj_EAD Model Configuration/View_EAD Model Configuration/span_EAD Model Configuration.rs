<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_EAD Model Configuration</name>
   <tag></tag>
   <elementGuidId>cdcaf57f-b8ac-41c1-b535-de77185fba02</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_5_1']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_5_1 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>518a1e6c-203e-4969-bf1a-cdeb8cf43c42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>21f92674-7a0d-48cd-a189-50c35545465e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>EAD Model Configuration</value>
      <webElementGuid>5cf094d8-29ce-4906-9ed3-b559f8caa8d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_5_1&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>06be3f6e-e4d6-4bf2-948e-1426578c1487</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_5_1']/span</value>
      <webElementGuid>28ea0a9b-5f2b-4957-a8b1-36472992e239</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='UD Configuration'])[1]/following::span[2]</value>
      <webElementGuid>39062d32-9edd-4897-a08b-6558747a12ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EAD Configuration'])[1]/following::span[4]</value>
      <webElementGuid>e3b33401-fe85-4c42-b377-bb956a3a2a1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PR Configuration'])[1]/preceding::span[2]</value>
      <webElementGuid>20704482-413e-4162-88c2-0d3a47cf002e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ECL Model Header'])[1]/preceding::span[4]</value>
      <webElementGuid>c8f317b5-a2e9-4e69-9578-72d3ba4eb5fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='EAD Model Configuration']/parent::*</value>
      <webElementGuid>c3d76f32-2956-48f9-98aa-e9954782beb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/ul/li[2]/a/span</value>
      <webElementGuid>1ded7ab1-8e1d-4494-8141-dd79e539a90a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'EAD Model Configuration' or . = 'EAD Model Configuration')]</value>
      <webElementGuid>cbf38dbb-6a19-4d72-98b6-46086e1c8007</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
