<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_UD Configuration</name>
   <tag></tag>
   <elementGuidId>153f4156-8302-4d83-8616-9dd28993b4db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_5_0']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_5_0 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7bbcb878-ea3c-4508-8597-15b17ea7caf0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>881fbbc0-0cec-4520-8861-8aafaff282a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>UD Configuration</value>
      <webElementGuid>0a700ccf-f391-4fb0-87fe-9f3aa783000c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_5_0&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>9434b47e-d5a1-4016-824d-5a5aa31a8f57</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_5_0']/span</value>
      <webElementGuid>247043ac-7ff2-473c-aff0-55c8b7d4361e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EAD Configuration'])[1]/following::span[2]</value>
      <webElementGuid>9686e66e-b11d-4042-92cd-110399369f9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LGD Configuration'])[1]/following::span[4]</value>
      <webElementGuid>925a3336-0f79-4d73-9cb8-8c6d154bee0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EAD Model Configuration'])[1]/preceding::span[2]</value>
      <webElementGuid>8592e03d-b68c-412e-9dba-a547bb1ffc2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PR Configuration'])[1]/preceding::span[4]</value>
      <webElementGuid>4a570c43-e513-4115-a74d-2dadff3a613c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='UD Configuration']/parent::*</value>
      <webElementGuid>cd2d8294-3ff5-4d91-b889-f15a1b5d9c23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/ul/li/a/span</value>
      <webElementGuid>e79b8fb9-e2c5-4257-af7f-7bc2dcbc98a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'UD Configuration' or . = 'UD Configuration')]</value>
      <webElementGuid>a8059a30-5f64-4448-80e1-d6ce3792accb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
