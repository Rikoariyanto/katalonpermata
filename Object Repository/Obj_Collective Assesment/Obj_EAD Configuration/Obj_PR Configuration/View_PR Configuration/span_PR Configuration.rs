<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_PR Configuration</name>
   <tag></tag>
   <elementGuidId>a8ed93b7-4e50-4e16-95ca-ce98b0b9e85b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_5_2']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_5_2 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ab236c02-6276-46c1-a3ee-9273a6b5f36b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>ab01e253-3fb9-4530-8e3e-6c7e577e91fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PR Configuration</value>
      <webElementGuid>abf0c09c-daa0-4261-a221-687cfa5f44a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_5_2&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>59b728c7-f125-46e6-b747-a587a87c4e44</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_5_2']/span</value>
      <webElementGuid>f6d6e806-6924-40f7-a9b0-d82b9e620fd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EAD Model Configuration'])[1]/following::span[2]</value>
      <webElementGuid>6f7f16d9-d0ba-4f05-8a2f-089355a678ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='UD Configuration'])[1]/following::span[4]</value>
      <webElementGuid>c88ff2bc-d36e-4fc7-bc88-368607b61efa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ECL Model Header'])[1]/preceding::span[2]</value>
      <webElementGuid>04405ac2-0b8c-449d-ac64-06127dc929ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pending Model Simulation'])[1]/preceding::span[4]</value>
      <webElementGuid>ad8960b1-9cf9-455b-9924-76c881ada019</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='PR Configuration']/parent::*</value>
      <webElementGuid>3ab90ead-fcec-4738-89f8-9640251cfe19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/ul/li[3]/a/span</value>
      <webElementGuid>87f89742-8645-40bb-b83d-645acd2cff26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'PR Configuration' or . = 'PR Configuration')]</value>
      <webElementGuid>51031989-b6de-4672-9124-2838555e29a9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
