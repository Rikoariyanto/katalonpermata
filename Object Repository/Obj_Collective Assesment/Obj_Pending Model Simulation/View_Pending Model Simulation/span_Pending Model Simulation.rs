<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Pending Model Simulation</name>
   <tag></tag>
   <elementGuidId>3fc153b4-4d4c-4b5a-8079-bc39e3b6383a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N3_7']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N3_7 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4c50755d-d321-4c94-a6df-40ed46a486f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>a940e7f1-9c8d-411a-a78c-57fa6846bd21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pending Model Simulation</value>
      <webElementGuid>08f72410-8ad6-4db5-b963-a783503857e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N3_7&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>fa0a5036-7ecc-4cde-b145-b418fbc2f414</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N3_7']/span</value>
      <webElementGuid>d2f08d33-0517-47e9-82b0-621c556b3f75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ECL Model Header'])[1]/following::span[2]</value>
      <webElementGuid>c9430d9a-c074-4174-82c8-2a232dcbc4a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PR Configuration'])[1]/following::span[4]</value>
      <webElementGuid>04b6e046-340a-43ab-baa3-244aa84c55e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reporting Workbench'])[1]/preceding::span[2]</value>
      <webElementGuid>1a525dfb-5712-4d48-a9fe-621d5380fe74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Report Analysis'])[1]/preceding::span[4]</value>
      <webElementGuid>88fc335e-9792-492a-b67f-b05f8b995589</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pending Model Simulation']/parent::*</value>
      <webElementGuid>3d593ded-d5eb-4d8e-85c7-1276fd924d48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/ul/li[8]/a/span</value>
      <webElementGuid>f25818d1-462f-4e2a-8d64-ed9897f0a270</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Pending Model Simulation' or . = 'Pending Model Simulation')]</value>
      <webElementGuid>316489d8-a4d4-4f20-8e79-9ddc8b0bcfd3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
