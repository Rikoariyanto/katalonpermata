<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Add</name>
   <tag></tag>
   <elementGuidId>158a0fd4-c560-4e58-bb30-5ee1a3a1c9ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ctContent_dgUser_cell0_0_lnkAdd&quot;]/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Obj_Parent/Obj_for_parent</value>
      <webElementGuid>6e282b29-6e56-4724-aad3-5667b50f1673</webElementGuid>
   </webElementProperties>
</WebElementEntity>
