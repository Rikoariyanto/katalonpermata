<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Save_btn</name>
   <tag></tag>
   <elementGuidId>1d5ed90a-462c-47ce-a5bc-4a89ddb01964</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ctContent_roundHeader_btnSave&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Obj_Parent/Obj_for_parent</value>
      <webElementGuid>f268a6ab-5649-4038-9c81-e0eddc84b645</webElementGuid>
   </webElementProperties>
</WebElementEntity>
