<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delete</name>
   <tag></tag>
   <elementGuidId>fd8a530b-0678-4146-a9b3-91115bdf5c67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ctContent_dgUser_cell0_0_btnDeleteImg&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Obj_Parent/Obj_for_parent</value>
      <webElementGuid>773f844a-e1ab-4da1-9218-8bba80a1151f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
