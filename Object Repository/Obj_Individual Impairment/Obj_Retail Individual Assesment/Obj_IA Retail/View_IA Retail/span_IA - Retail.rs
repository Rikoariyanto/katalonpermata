<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_IA - Retail</name>
   <tag></tag>
   <elementGuidId>98a01c2a-f4f9-4371-90a5-5dbbf632d3ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N2_1_0']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N2_1_0 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5222058c-ebde-470a-b00f-12be3837b9c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>591f2ad3-9d18-47ae-a26f-4e57469389b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>IA - Retail</value>
      <webElementGuid>85ceafd5-9035-4720-99ce-fb7adf7a756a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N2_1_0&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>aa23ed8a-779f-4f9f-a4fe-7b5f1f32e76c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N2_1_0']/span</value>
      <webElementGuid>6f00ef26-96b6-4e90-8a4c-1e28171b5384</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Retail Individual Assesment'])[1]/following::span[2]</value>
      <webElementGuid>f6cfdeb6-e85a-4aeb-bfbc-cb3d4f407c62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='IA - SAM History'])[1]/following::span[4]</value>
      <webElementGuid>3e9cd30b-18e6-4aab-bce9-265d6e5e4327</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collective Assessment'])[1]/preceding::span[2]</value>
      <webElementGuid>91bea6a9-3a7e-4d0b-ab88-f99785f4ce35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Segmentation Configuration'])[1]/preceding::span[4]</value>
      <webElementGuid>09b3dc83-0850-40f9-b2b9-74e83737e534</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='IA - Retail']/parent::*</value>
      <webElementGuid>96ac1a6b-6b73-4c0c-9433-4c2a39d6b67e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/ul/li[2]/ul/li/a/span</value>
      <webElementGuid>8a762d7a-26c5-430e-9691-1fe7d8326a4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'IA - Retail' or . = 'IA - Retail')]</value>
      <webElementGuid>93938ae2-d4f1-4ec9-a66c-61b8966b73de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
