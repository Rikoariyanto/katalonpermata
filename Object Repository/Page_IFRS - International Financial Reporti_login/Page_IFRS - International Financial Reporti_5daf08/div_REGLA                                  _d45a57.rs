<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_REGLA                                  _d45a57</name>
   <tag></tag>
   <elementGuidId>e881548a-328d-41fd-bf13-b2abc72a0f5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form1']/div[3]/header/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-sm-6</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>136a2b47-964d-48c5-ae17-8c354cc62229</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-6</value>
      <webElementGuid>ca689efe-a42d-44d8-bb73-f1aca07a8c0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            
                        
                        
                            
                            
                            REGLA
                        
                        PSAK 71
                    </value>
      <webElementGuid>d979a5ea-9a43-412c-9433-a81d2951a14a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form1&quot;)/div[3]/header[@class=&quot;bd-navbar navbar-expand-lg navbar-dark bg-dark&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-6&quot;]</value>
      <webElementGuid>554996b2-cf07-4dc8-b367-84a1021a4b00</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form1']/div[3]/header/div/div</value>
      <webElementGuid>6ad3ceed-4e25-4e29-a9d8-e515ccde4416</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>93eae149-5607-472a-8586-f850f518a196</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            
                        
                        
                            
                            
                            REGLA
                        
                        PSAK 71
                    ' or . = '
                        
                            
                        
                        
                            
                            
                            REGLA
                        
                        PSAK 71
                    ')]</value>
      <webElementGuid>73b3b62d-6835-4d86-99e7-6fc176ae75cb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
