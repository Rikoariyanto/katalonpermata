<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Master Retention</name>
   <tag></tag>
   <elementGuidId>43a6c9b7-ce77-40af-ade0-03cdfa985bc2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N0_3']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N0_3 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>41bfcf64-c569-4739-8847-8ecb4b10f9e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>9d5ab288-dc2e-46f7-bd49-aff9f835056d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Master Retention</value>
      <webElementGuid>8358a79b-feec-4c76-889c-d07f6a255b8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N0_3&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>3f927cfa-160d-4ccc-8d69-c137843e4dda</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N0_3']/span</value>
      <webElementGuid>f606b18e-7e3b-466c-939b-d3ff877b8e82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Security Settings'])[1]/following::span[2]</value>
      <webElementGuid>1ffc44b1-d5ce-43d5-b251-22539a075def</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Business Settings'])[1]/following::span[4]</value>
      <webElementGuid>e2c44bfb-c88d-46f2-bfa2-feecd1339c6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Scenario Rule Setting'])[1]/preceding::span[2]</value>
      <webElementGuid>d428a626-4300-42d9-b03e-eb8394008d97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Parameter Setup'])[1]/preceding::span[4]</value>
      <webElementGuid>ca0a9353-f1c5-4924-a8f5-020e309e5054</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Master Retention']/parent::*</value>
      <webElementGuid>0aa29516-afb0-470d-8e89-7d52f4fe7565</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a/span</value>
      <webElementGuid>28e6b564-01d8-4508-9d0a-369d241b4780</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Master Retention' or . = 'Master Retention')]</value>
      <webElementGuid>71ea41cd-adb8-4570-9c8f-ac897ba1856a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
