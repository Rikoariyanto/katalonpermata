<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Scenario Rule Setting</name>
   <tag></tag>
   <elementGuidId>ea385142-0bca-492b-aa59-1193ed54e963</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N0_4']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N0_4 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a19438a4-69de-4519-bb44-3c48ec3dc145</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>25ace4d3-bd66-496b-94c3-e16af03f614c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Scenario Rule Setting</value>
      <webElementGuid>7d7ec428-ea8f-4286-8b62-468b7a356296</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N0_4&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>c55a8b5d-2387-4d2f-82ff-9694b4c3195b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N0_4']/span</value>
      <webElementGuid>d3cfb92b-2d65-4607-b9da-719efe9baec2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Master Retention'])[1]/following::span[2]</value>
      <webElementGuid>e2505daf-6f8c-4b4a-ada8-637d334f4625</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Security Settings'])[1]/following::span[4]</value>
      <webElementGuid>63eee372-9137-4aa0-b4ee-92d6b6e068ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Parameter Setup'])[1]/preceding::span[2]</value>
      <webElementGuid>ce805568-ff52-4fca-b9e5-ee887fcf7b3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Product Parameter'])[1]/preceding::span[4]</value>
      <webElementGuid>901e934c-273f-496e-abec-f67f2830eaac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Scenario Rule Setting']/parent::*</value>
      <webElementGuid>205d6544-def5-4990-922f-86814a3c520a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/a/span</value>
      <webElementGuid>4ae3e98b-a5d5-4942-beaf-4b912f72b4b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Scenario Rule Setting' or . = 'Scenario Rule Setting')]</value>
      <webElementGuid>affba063-d5c5-4e10-90c1-fa4304e6d97e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
