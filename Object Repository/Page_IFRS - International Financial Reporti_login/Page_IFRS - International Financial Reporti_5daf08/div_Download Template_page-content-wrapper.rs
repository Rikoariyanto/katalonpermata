<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Download Template_page-content-wrapper</name>
   <tag></tag>
   <elementGuidId>03a0ace8-5ac9-4805-af6a-ec7c62f9c6ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-content-wrapper']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#page-content-wrapper</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b2060c23-73a0-4e86-9f7c-7288088d954a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>page-content-wrapper</value>
      <webElementGuid>f46bb05c-39c5-42f1-8abd-e2fc45798354</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-content-wrapper&quot;)</value>
      <webElementGuid>29cc71b9-f7a0-4961-a827-f80f2be2b2f5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='page-content-wrapper']</value>
      <webElementGuid>37262652-7604-4921-bf6d-84cba3ae9361</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wrapper']/div[2]</value>
      <webElementGuid>c9301d6a-a65d-49da-aeca-fbb05dee86a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download Template'])[1]/following::div[1]</value>
      <webElementGuid>ad8de384-56d0-4d5e-a4e4-4cfee5033db8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Help'])[1]/following::div[1]</value>
      <webElementGuid>368d6bec-bce6-4868-97a3-a4ee0705e0c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'page-content-wrapper', '&quot;', ')')])[1]/preceding::div[4]</value>
      <webElementGuid>496d13b4-99a3-4b62-89bd-0e571b06208d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]</value>
      <webElementGuid>c33facfe-14f2-45a8-b84d-e3f6e775dd80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'page-content-wrapper']</value>
      <webElementGuid>9cd851b2-3ef4-4ada-a4d1-1db01c768fcb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
