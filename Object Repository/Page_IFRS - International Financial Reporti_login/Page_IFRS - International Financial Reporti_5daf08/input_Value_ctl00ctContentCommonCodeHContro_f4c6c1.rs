<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Value_ctl00ctContentCommonCodeHContro_f4c6c1</name>
   <tag></tag>
   <elementGuidId>49692bdf-b0cf-4f7a-b027-c02b6ec422e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2_I</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2_I']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>730c5bd0-aabc-42f8-894f-c2da5e696998</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxeEditArea_Material dxeEditAreaSys dxh0</value>
      <webElementGuid>aaf2e955-2e7d-48df-9dea-cf1dc8a75248</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2_I</value>
      <webElementGuid>4e34c644-e443-4167-8c44-6ec0be788706</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>ctl00$ctContent$CommonCodeHControl1$uclGrid$dgData$DXFREditorcol2</value>
      <webElementGuid>8f6146cc-8522-4aae-b26b-0330d684ceea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onfocus</name>
      <type>Main</type>
      <value>ASPx.EGotFocus('ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2')</value>
      <webElementGuid>580ae2fd-d5e8-43d6-980e-550a82d79d35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onblur</name>
      <type>Main</type>
      <value>ASPx.ELostFocus('ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2')</value>
      <webElementGuid>d963fef8-1ba6-4208-b0b9-4e615f0bc65e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>ASPx.EValueChanged('ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2')</value>
      <webElementGuid>3b73ad1d-ec3d-4b43-adaa-bf4ac7d90eb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>18fa2d3a-5c80-4376-abca-f849b99577e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2_I&quot;)</value>
      <webElementGuid>c7fcf059-3492-4ce3-b320-558be8b064eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_IFRS - International Financial Reporti_5daf08/iframe_Download Template_ifrcontent</value>
      <webElementGuid>ca68f709-9454-4159-9726-9d2a39db563f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2_I']</value>
      <webElementGuid>69174ffd-3c4f-4f25-b96d-3cb29dc92068</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2']/tbody/tr/td/input</value>
      <webElementGuid>dbdd5561-e721-4900-80c3-ae39878d7587</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/table/tbody/tr/td/input</value>
      <webElementGuid>a6c20bf3-529e-4938-a4bf-cbd9a9d3670e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'ctl00_ctContent_CommonCodeHControl1_uclGrid_dgData_DXFREditorcol2_I' and @name = 'ctl00$ctContent$CommonCodeHControl1$uclGrid$dgData$DXFREditorcol2' and @type = 'text']</value>
      <webElementGuid>b42466d9-1902-4b0a-897d-5804e8f43d40</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
