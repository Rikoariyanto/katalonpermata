<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ul_Home                                    _a3a3ad</name>
   <tag></tag>
   <elementGuidId>45ea8fb6-a92c-45d3-b7fe-5554709e59f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='sidebar-wrapper']/ul</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.sidebar-nav</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>a4ba5835-7713-4d1a-acee-a4687e409cbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-nav</value>
      <webElementGuid>49d546ae-c8dd-46f3-9fda-ed7d66f910a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            
                                
                                      
                                    Home
                                
                            
                        
                        
                            
                            
                                
	
		
			
				General Setup
			
				Application SettingsBusiness SettingsSecurity SettingsMaster RetentionScenario Rule Setting
			
				Parameter Setup
			
				Product ParameterTransaction ParameterJournal ParameterMarket Rate ParameterJoint Finance Portion Parameter
			
				Classification and Measurement
			
				
					SPPI Setup
				
					SPPI Section SetupSPPI Question Setup
				Business Model SetupSPPI ClassificationBusiness Model ClassificationAsset ClassificationAsset Classification OverrideAsset Classification Corporate
			
				Impairment
			
				
					Individual Assessment
				
					Individual Assesment Override
				
					Collective Assessment
				
					Segmentation ConfigurationGroup Bucket MaintenancePD ConfigurationLGD Configuration
						Forward Looking Model
					
						Variable RegistrationVariable ForecastVariable SelectionModel SelectionScalar Configuration
					
						EAD Model
					
						CCF ConfigurationEAD Configuration
					ECL Wizard
				
			
				Reporting Parameter Setup
			
				Segment PSAK55SMBC Class ParameterSegment ExposureSegment LoansIndustry SectorsGeographical Sectors
			
				Reporting Workbench
			
				
					Asset
				
					Loan ModuleLoan Report ReconLoan Transaction Cost MonitoringEIR and SL Dashboard AssetTrade ModuleTreasury ModuleAmortization Facility HeaderAmortization Facility Detail
				
					Asset Impairment
				
					PD AnalysisLGD AnalysisCCF AnalysisObserved Default Rate AnalysisAverage EIRECL Model ResultIA Cashflow MonitoringIA Monitoring ReportIA Provision BreakdownBacktest ResultPD After Forward Looking
				
					Liabilities
				
					Funding ModuleFunding Report ReconFunding Transaction Cost MonitoringEIR and SL Dashboard Liabilities
				
			
				Maintenance
			
				User ManagementScreen ManagementApproval
					Exception
				
					Unprocessed Account
				
			
				Audit and History
			
				User Activity ReportApproval Report
			
				Tools
			
				
					Manual Upload
				
					Mapping RulesManual Upload Files
				
					Manual Upload Transaction
				
					Manual Upload Transaction Cost AssetManual Upload Transaction Cost LiabilitiesManual Upload Facility Fee/Cost
				
					Manual Upload Transaction Monitoring
				
					Manual Upload Transaction Cost Asset MonitoringManual Upload Transaction Cost Liabilities MonitoringManual Upload Facility Fee/Cost Monitoring
				Query AdhocPSAK 71 Staging ProcessPSAK 71 Daily/Monthly ProcessMonitoring Process
			
				Help
			
				Download Template
			
		
	

&lt;!--
ASPx.AddHoverItems('treeView',[[['dxtv-ndHov'],[''],['N0','N0_0','N0_1','N0_2','N0_3','N0_4','N1','N1_0','N1_1','N1_2','N1_3','N1_4','N2','N2_0','N2_0_0','N2_0_1','N2_1','N2_2','N2_3','N2_4','N2_5','N2_6','N3','N3_0','N3_0_0','N3_1','N3_1_0','N3_1_1','N3_1_2','N3_1_3','N3_1_4','N3_1_4_0','N3_1_4_1','N3_1_4_2','N3_1_4_3','N3_1_4_4','N3_1_5','N3_1_5_0','N3_1_5_1','N3_1_6','N4','N4_0','N4_1','N4_2','N4_3','N4_4','N4_5','N5','N5_0','N5_0_0','N5_0_1','N5_0_2','N5_0_3','N5_0_4','N5_0_5','N5_0_6','N5_0_7','N5_1','N5_1_0','N5_1_1','N5_1_2','N5_1_3','N5_1_4','N5_1_5','N5_1_6','N5_1_7','N5_1_8','N5_1_9','N5_1_10','N5_2','N5_2_0','N5_2_1','N5_2_2','N5_2_3','N6','N6_0','N6_1','N6_2','N6_3','N6_3_0','N7','N7_0','N7_1','N8','N8_0','N8_0_0','N8_0_1','N8_1','N8_1_0','N8_1_1','N8_1_2','N8_2','N8_2_0','N8_2_1','N8_2_2','N8_3','N8_4','N8_5','N8_6','N9','N9_0']]]);
ASPx.AddSelectedItems('treeView',[[['dxtv-ndSel'],[''],['N0','N0_0','N0_1','N0_2','N0_3','N0_4','N1','N1_0','N1_1','N1_2','N1_3','N1_4','N2','N2_0','N2_0_0','N2_0_1','N2_1','N2_2','N2_3','N2_4','N2_5','N2_6','N3','N3_0','N3_0_0','N3_1','N3_1_0','N3_1_1','N3_1_2','N3_1_3','N3_1_4','N3_1_4_0','N3_1_4_1','N3_1_4_2','N3_1_4_3','N3_1_4_4','N3_1_5','N3_1_5_0','N3_1_5_1','N3_1_6','N4','N4_0','N4_1','N4_2','N4_3','N4_4','N4_5','N5','N5_0','N5_0_0','N5_0_1','N5_0_2','N5_0_3','N5_0_4','N5_0_5','N5_0_6','N5_0_7','N5_1','N5_1_0','N5_1_1','N5_1_2','N5_1_3','N5_1_4','N5_1_5','N5_1_6','N5_1_7','N5_1_8','N5_1_9','N5_1_10','N5_2','N5_2_0','N5_2_1','N5_2_2','N5_2_3','N6','N6_0','N6_1','N6_2','N6_3','N6_3_0','N7','N7_0','N7_1','N8','N8_0','N8_0_0','N8_0_1','N8_1','N8_1_0','N8_1_1','N8_1_2','N8_2','N8_2_0','N8_2_1','N8_2_2','N8_3','N8_4','N8_5','N8_6','N9','N9_0']]]);

var dxo = new ASPxClientTreeView('treeView');
dxo.InitGlobalVariable('treeView');
dxo.SetProperties({
	'nodesState':[
		{
			'N3':'',
			'N2':'',
			'N5':'',
			'N9':'',
			'N5_0':'',
			'N3_1_5':'',
			'N7':'',
			'N6':'',
			'N3_0':'',
			'N8_1':'',
			'N5_2':'',
			'N3_1':'',
			'N6_3':'',
			'N0':'',
			'N8_2':'',
			'N4':'',
			'N2_0':'',
			'N8':'',
			'N5_1':'',
			'N3_1_4':'',
			'N1':'',
			'N8_0':''
		},
		'',
		{}
	],
	'nodesUrls':{},
	'allowSelectNode':true
});
dxo.AfterCreate();

//-->

                            
                        
                    </value>
      <webElementGuid>41a101de-4918-4a4b-ab37-7d9ba0a6fb05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-nav&quot;]</value>
      <webElementGuid>1f684896-a6e0-4d3a-a136-4df3b74c3e86</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='sidebar-wrapper']/ul</value>
      <webElementGuid>135ca9e6-6e78-4887-a825-16aae2265a72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/following::ul[1]</value>
      <webElementGuid>bbd9eb54-85d3-45a0-a649-d1fb87a7e9ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/ul</value>
      <webElementGuid>f9e0e0c0-aace-4216-bf5f-771cc98538a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[(text() = concat(&quot;
                        
                            
                                
                                      
                                    Home
                                
                            
                        
                        
                            
                            
                                
	
		
			
				General Setup
			
				Application SettingsBusiness SettingsSecurity SettingsMaster RetentionScenario Rule Setting
			
				Parameter Setup
			
				Product ParameterTransaction ParameterJournal ParameterMarket Rate ParameterJoint Finance Portion Parameter
			
				Classification and Measurement
			
				
					SPPI Setup
				
					SPPI Section SetupSPPI Question Setup
				Business Model SetupSPPI ClassificationBusiness Model ClassificationAsset ClassificationAsset Classification OverrideAsset Classification Corporate
			
				Impairment
			
				
					Individual Assessment
				
					Individual Assesment Override
				
					Collective Assessment
				
					Segmentation ConfigurationGroup Bucket MaintenancePD ConfigurationLGD Configuration
						Forward Looking Model
					
						Variable RegistrationVariable ForecastVariable SelectionModel SelectionScalar Configuration
					
						EAD Model
					
						CCF ConfigurationEAD Configuration
					ECL Wizard
				
			
				Reporting Parameter Setup
			
				Segment PSAK55SMBC Class ParameterSegment ExposureSegment LoansIndustry SectorsGeographical Sectors
			
				Reporting Workbench
			
				
					Asset
				
					Loan ModuleLoan Report ReconLoan Transaction Cost MonitoringEIR and SL Dashboard AssetTrade ModuleTreasury ModuleAmortization Facility HeaderAmortization Facility Detail
				
					Asset Impairment
				
					PD AnalysisLGD AnalysisCCF AnalysisObserved Default Rate AnalysisAverage EIRECL Model ResultIA Cashflow MonitoringIA Monitoring ReportIA Provision BreakdownBacktest ResultPD After Forward Looking
				
					Liabilities
				
					Funding ModuleFunding Report ReconFunding Transaction Cost MonitoringEIR and SL Dashboard Liabilities
				
			
				Maintenance
			
				User ManagementScreen ManagementApproval
					Exception
				
					Unprocessed Account
				
			
				Audit and History
			
				User Activity ReportApproval Report
			
				Tools
			
				
					Manual Upload
				
					Mapping RulesManual Upload Files
				
					Manual Upload Transaction
				
					Manual Upload Transaction Cost AssetManual Upload Transaction Cost LiabilitiesManual Upload Facility Fee/Cost
				
					Manual Upload Transaction Monitoring
				
					Manual Upload Transaction Cost Asset MonitoringManual Upload Transaction Cost Liabilities MonitoringManual Upload Facility Fee/Cost Monitoring
				Query AdhocPSAK 71 Staging ProcessPSAK 71 Daily/Monthly ProcessMonitoring Process
			
				Help
			
				Download Template
			
		
	

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;treeView&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxtv-ndHov&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;N0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_3_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N9_0&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;treeView&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxtv-ndSel&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;N0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_3_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N9_0&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientTreeView(&quot; , &quot;'&quot; , &quot;treeView&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;treeView&quot; , &quot;'&quot; , &quot;);
dxo.SetProperties({
	&quot; , &quot;'&quot; , &quot;nodesState&quot; , &quot;'&quot; , &quot;:[
		{
			&quot; , &quot;'&quot; , &quot;N3&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N2&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N5&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N9&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N5_0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N3_1_5&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N7&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N6&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N3_0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N8_1&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N5_2&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N3_1&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N6_3&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N8_2&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N4&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N2_0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N8&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N5_1&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N3_1_4&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N1&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N8_0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
		},
		&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
		{}
	],
	&quot; , &quot;'&quot; , &quot;nodesUrls&quot; , &quot;'&quot; , &quot;:{},
	&quot; , &quot;'&quot; , &quot;allowSelectNode&quot; , &quot;'&quot; , &quot;:true
});
dxo.AfterCreate();

//-->

                            
                        
                    &quot;) or . = concat(&quot;
                        
                            
                                
                                      
                                    Home
                                
                            
                        
                        
                            
                            
                                
	
		
			
				General Setup
			
				Application SettingsBusiness SettingsSecurity SettingsMaster RetentionScenario Rule Setting
			
				Parameter Setup
			
				Product ParameterTransaction ParameterJournal ParameterMarket Rate ParameterJoint Finance Portion Parameter
			
				Classification and Measurement
			
				
					SPPI Setup
				
					SPPI Section SetupSPPI Question Setup
				Business Model SetupSPPI ClassificationBusiness Model ClassificationAsset ClassificationAsset Classification OverrideAsset Classification Corporate
			
				Impairment
			
				
					Individual Assessment
				
					Individual Assesment Override
				
					Collective Assessment
				
					Segmentation ConfigurationGroup Bucket MaintenancePD ConfigurationLGD Configuration
						Forward Looking Model
					
						Variable RegistrationVariable ForecastVariable SelectionModel SelectionScalar Configuration
					
						EAD Model
					
						CCF ConfigurationEAD Configuration
					ECL Wizard
				
			
				Reporting Parameter Setup
			
				Segment PSAK55SMBC Class ParameterSegment ExposureSegment LoansIndustry SectorsGeographical Sectors
			
				Reporting Workbench
			
				
					Asset
				
					Loan ModuleLoan Report ReconLoan Transaction Cost MonitoringEIR and SL Dashboard AssetTrade ModuleTreasury ModuleAmortization Facility HeaderAmortization Facility Detail
				
					Asset Impairment
				
					PD AnalysisLGD AnalysisCCF AnalysisObserved Default Rate AnalysisAverage EIRECL Model ResultIA Cashflow MonitoringIA Monitoring ReportIA Provision BreakdownBacktest ResultPD After Forward Looking
				
					Liabilities
				
					Funding ModuleFunding Report ReconFunding Transaction Cost MonitoringEIR and SL Dashboard Liabilities
				
			
				Maintenance
			
				User ManagementScreen ManagementApproval
					Exception
				
					Unprocessed Account
				
			
				Audit and History
			
				User Activity ReportApproval Report
			
				Tools
			
				
					Manual Upload
				
					Mapping RulesManual Upload Files
				
					Manual Upload Transaction
				
					Manual Upload Transaction Cost AssetManual Upload Transaction Cost LiabilitiesManual Upload Facility Fee/Cost
				
					Manual Upload Transaction Monitoring
				
					Manual Upload Transaction Cost Asset MonitoringManual Upload Transaction Cost Liabilities MonitoringManual Upload Facility Fee/Cost Monitoring
				Query AdhocPSAK 71 Staging ProcessPSAK 71 Daily/Monthly ProcessMonitoring Process
			
				Help
			
				Download Template
			
		
	

&lt;!--
ASPx.AddHoverItems(&quot; , &quot;'&quot; , &quot;treeView&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxtv-ndHov&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;N0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_3_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N9_0&quot; , &quot;'&quot; , &quot;]]]);
ASPx.AddSelectedItems(&quot; , &quot;'&quot; , &quot;treeView&quot; , &quot;'&quot; , &quot;,[[[&quot; , &quot;'&quot; , &quot;dxtv-ndSel&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;],[&quot; , &quot;'&quot; , &quot;N0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N0_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N2_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_4_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_5_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N3_1_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N4_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_0_7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_1_10&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N5_2_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N6_3_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N7_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_0_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_1_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_0&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_1&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_2_2&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_3&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_4&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_5&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N8_6&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N9&quot; , &quot;'&quot; , &quot;,&quot; , &quot;'&quot; , &quot;N9_0&quot; , &quot;'&quot; , &quot;]]]);

var dxo = new ASPxClientTreeView(&quot; , &quot;'&quot; , &quot;treeView&quot; , &quot;'&quot; , &quot;);
dxo.InitGlobalVariable(&quot; , &quot;'&quot; , &quot;treeView&quot; , &quot;'&quot; , &quot;);
dxo.SetProperties({
	&quot; , &quot;'&quot; , &quot;nodesState&quot; , &quot;'&quot; , &quot;:[
		{
			&quot; , &quot;'&quot; , &quot;N3&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N2&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N5&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N9&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N5_0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N3_1_5&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N7&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N6&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N3_0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N8_1&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N5_2&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N3_1&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N6_3&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N8_2&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N4&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N2_0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N8&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N5_1&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N3_1_4&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N1&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
			&quot; , &quot;'&quot; , &quot;N8_0&quot; , &quot;'&quot; , &quot;:&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;
		},
		&quot; , &quot;'&quot; , &quot;&quot; , &quot;'&quot; , &quot;,
		{}
	],
	&quot; , &quot;'&quot; , &quot;nodesUrls&quot; , &quot;'&quot; , &quot;:{},
	&quot; , &quot;'&quot; , &quot;allowSelectNode&quot; , &quot;'&quot; , &quot;:true
});
dxo.AfterCreate();

//-->

                            
                        
                    &quot;))]</value>
      <webElementGuid>fb3e1c47-a7d6-49b1-b104-b220f582789c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
