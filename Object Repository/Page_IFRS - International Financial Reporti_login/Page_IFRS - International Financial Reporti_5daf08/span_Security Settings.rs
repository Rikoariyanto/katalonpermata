<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Security Settings</name>
   <tag></tag>
   <elementGuidId>cc3708f1-0d46-4660-b7b0-f84095d8f6f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N0_2']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#treeView_N0_2 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>649bf5e5-c10d-4220-a25e-2273c8c8d1a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>b49119ce-19d9-454f-a820-fef25db84a94</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Security Settings</value>
      <webElementGuid>a6156940-ad45-4a92-a746-b4b7cf119167</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N0_2&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>8eab8687-1a1e-4dc4-a8d9-6d5417313dd1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N0_2']/span</value>
      <webElementGuid>adebf6c6-4ab3-47cf-bea4-dbd300b1a90d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Business Settings'])[1]/following::span[2]</value>
      <webElementGuid>30d2a1b2-1511-4cfa-b179-eba9f6e4f52e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Application Settings'])[1]/following::span[4]</value>
      <webElementGuid>0aefc72c-1a53-48be-8818-3d3e0fec0abc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Master Retention'])[1]/preceding::span[2]</value>
      <webElementGuid>a2dd04c2-2d29-4937-9013-f790ba53c681</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Scenario Rule Setting'])[1]/preceding::span[4]</value>
      <webElementGuid>1ad3c0b1-5b2f-432e-8855-c0e3d63cc979</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Security Settings']/parent::*</value>
      <webElementGuid>39879b1a-ff48-4b4c-8509-2338c5e5a521</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/ul/li[3]/a/span</value>
      <webElementGuid>896a4b9d-e720-47f5-8bc0-55c485fbb094</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Security Settings' or . = 'Security Settings')]</value>
      <webElementGuid>1bb0b1ed-4cc4-4283-93b3-57aa5aa911ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
