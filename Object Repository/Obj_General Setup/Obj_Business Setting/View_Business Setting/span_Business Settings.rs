<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Business Settings</name>
   <tag></tag>
   <elementGuidId>3da26d92-fa16-4473-9305-3d7366bc268e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#treeView_N0_0 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N0_0']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>45cdcc92-fc84-4094-9b9c-4eb679bf6d3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>8a29c7b7-d4d0-4b34-946c-8bb69dc57397</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Business Settings</value>
      <webElementGuid>a1400d0a-fb70-43d2-90f1-b58d7206c273</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N0_0&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>1388913f-e11c-4b49-bd54-77b1c25f53f3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N0_0']/span</value>
      <webElementGuid>edab21f8-27c4-4cb3-867e-733bf2a07e44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='General Setup'])[1]/following::span[2]</value>
      <webElementGuid>93a6b7a7-345a-43bf-ba04-19d9295f6eb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/following::span[4]</value>
      <webElementGuid>39aed576-882f-4825-83d1-6bde53302f72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Security Settings'])[1]/preceding::span[2]</value>
      <webElementGuid>7e43cb6f-8e21-498c-b9af-8a3fb6a2f069</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Master Retention'])[1]/preceding::span[4]</value>
      <webElementGuid>78287517-f692-4f68-bc2b-a7a75ad543d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Business Settings']/parent::*</value>
      <webElementGuid>ae9859bf-c21d-4f8a-b23b-ace9d25af321</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/ul/li/a/span</value>
      <webElementGuid>1098e08d-ebb3-4f82-9230-8b44ddf96324</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Business Settings' or . = 'Business Settings')]</value>
      <webElementGuid>e1ab1bbf-d44b-460e-abd3-a2093669f9a2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
