<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_export</name>
   <tag></tag>
   <elementGuidId>f1a5e833-bb7d-4bc4-be90-ddb2b467899f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;ctl00_ctContent_CommonCodeHControl1_uclGrid_uclExporter_lnkExportXLS_dgData&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Obj_Parent/Obj_for_parent</value>
      <webElementGuid>2d608eb6-1711-4373-b67e-9571e78f512b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
