<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Security Settings</name>
   <tag></tag>
   <elementGuidId>55f8c4a9-cd1d-4edf-b22e-846ee5de5334</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#treeView_N0_1 > span.dxtv-ndTxt.dx-vam</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='treeView_N0_1']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>50c8da17-1d6d-4089-bd4f-190ff99332d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dxtv-ndTxt dx-vam</value>
      <webElementGuid>e52faa16-090d-4978-8aad-1cec52a83ff0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Security Settings</value>
      <webElementGuid>7e743cab-fe2c-41f8-9cd3-ddf00e5c20ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;treeView_N0_1&quot;)/span[@class=&quot;dxtv-ndTxt dx-vam&quot;]</value>
      <webElementGuid>4ab401aa-9316-48ea-88b8-0afc37eb4e76</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='treeView_N0_1']/span</value>
      <webElementGuid>4869463d-1836-4a7b-99cc-330548045641</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Business Settings'])[1]/following::span[2]</value>
      <webElementGuid>25af21f9-6e32-402a-8456-882fc84f3dd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='General Setup'])[1]/following::span[4]</value>
      <webElementGuid>9c372d1f-01f7-41fb-af36-19b02decc9c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Master Retention'])[1]/preceding::span[2]</value>
      <webElementGuid>8a1b3a67-2abc-4607-8b4b-a6827638a339</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Scenario Rule Setting'])[1]/preceding::span[4]</value>
      <webElementGuid>e78ac4aa-55f5-407b-986a-42a5c29ad571</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Security Settings']/parent::*</value>
      <webElementGuid>e4b7b613-b86a-41a2-a18a-f5113b7b3d80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/ul/li[2]/a/span</value>
      <webElementGuid>b564b244-2703-42eb-ad32-bebd7ccdca3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Security Settings' or . = 'Security Settings')]</value>
      <webElementGuid>f508eb08-d36c-46b7-bb28-4950a7978a08</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
